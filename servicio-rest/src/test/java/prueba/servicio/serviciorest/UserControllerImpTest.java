package prueba.servicio.serviciorest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import prueba.servicio.serviciorest.controller.UserControllerImp;
import prueba.servicio.serviciorest.model.UserModel;
import prueba.servicio.serviciorest.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserControllerImpTest {

    @MockBean
    UserService service;

    @Autowired
    UserControllerImp userController;

    @Test
    public void testGetUsers() {
        List<UserModel> list = new ArrayList<>();
        list.add(new UserModel(1,"user", "one"));
        list.add(new UserModel(2,"user", "two"));
        list.add(new UserModel(3,"user", "three"));
        when(this.service.getUsers()).thenReturn(list);

        ResponseEntity<List<UserModel>> response = userController.getUsers();
        Assertions.assertNotNull(response);
        Assertions.assertEquals(3, Objects.requireNonNull(response.getBody()).size());
    }

    @Test
    public void testCreateUser() {
        UserModel user = new UserModel(1,"user", "one");

        ResponseEntity<String> response = userController.createUser(user);

        Mockito.verify(service, Mockito.times(1)).saveUser(eq(user));
    }
}
