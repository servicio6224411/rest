package prueba.servicio.serviciorest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import prueba.servicio.serviciorest.model.UserModel;
import prueba.servicio.serviciorest.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository repository;

    public List<UserModel> getUsers() {
        return new ArrayList<>((Collection<? extends UserModel>) repository.findAll());
    }

    public void saveUser(UserModel userModel) {
        repository.save(userModel);
    }
}
