package prueba.servicio.serviciorest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import prueba.servicio.serviciorest.model.UserModel;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Integer> {
}
