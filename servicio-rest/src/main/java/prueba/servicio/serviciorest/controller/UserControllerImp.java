package prueba.servicio.serviciorest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import prueba.servicio.serviciorest.model.UserModel;
import prueba.servicio.serviciorest.service.UserService;

import java.util.List;

@RestController
public class UserControllerImp implements UserController {

    @Autowired
    UserService service;

    @Override
    public ResponseEntity<List<UserModel>> getUsers() {
        List<UserModel> list;
        try {
            list = service.getUsers();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> createUser(UserModel user) {
        try {
            service.saveUser(user);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
}
